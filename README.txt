This module provides a wrapper around the Form Builder module which
allows you to store Form Builder created forms in the database, export
them to code and register a path at which they will display.

This module does not do any handling of the submitted form data. To do
that you will need to create a form processing module. This can be as simple
as creating a module that hook_form_alter()'s your new form to add 
validate and submit functions contained in your new module. Alternately,
you can simply use the Form API code generated here for use in another
module entirely.


TODO
- Use of Ctools mechanisms to allow for importing code based form data or 
  document that is something left to the features module to handle for example
  when viewing form
- error message displays about missing form_manager_form.tpl.php
- Form fieldsets do not display as fieldsets (display as buttons)
  - This is the result of using fieldsets as the delimiter for "pages" in
    multistep forms. The buttons are created as naviation to that "page".
  - Fieldsets within a fieldset will function as expected.
- need validation on the form_id to prevent dashes
- no apparent difference between the new number type and the new textfield type
- No way to edit a form to publish it or change whether it is multistep after 
  creation
  - The multistep form option is not currently implemented. All forms are 
    assumed to be multistep if elements are grouped within fieldsets.
  - Creating a form and exporting the Forms API code for use in another module
    will not use this multistep functionality, and so fieldsets will be preserved.
