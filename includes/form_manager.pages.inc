<?php

/**
 * Transform the single-step form into a mult-step form by breaking apart the outer-layer of 
 * fieldsets (leaving nested fieldsets alone). Adds the necesary steps, storage, etc. to make
 * it perform as a typical multi-step form.
 *
 * @param $form_state
 *   The current form state. Since this function may be called while stepping through the form, 
 *   it's necesary to keep up with all the pieces.
 *
 * @return
 *   The multi-step form array.
 */ 
function form_manager_form(&$form_state, $form_id) {

  // ensure step is set in storage
  if (empty($form_state['storage']['#navigation']['step'])) {
    $form_state['storage']['#navigation']['step'] = 1;
    //$form_state['storage']['#navigation']['newStep'] = $form_state['storage']['#navigation']['step'] + 1;
  }
  elseif ($form_state['storage']['#navigation']['newStep']) {
    // if the form has been submitted, check for a new step
    $form_state['storage']['#navigation']['step'] = $form_state['storage']['#navigation']['newStep'];
    unset ($form_state['storage']['#navigation']['newStep']);
  }

  $form = array();

  // add form fields
  _form_manager_form_add_fields($form, $form_state, $form_id);

  // calculate max number of steps
  _form_manager_form_max_steps($form, $form_state);

  // create tabs
  _form_manager_form_create_tabs($form, $form_state);

  // remove form fields per step
  _form_manager_form_remove_fields($form, $form_state);

  // Once the fieldset has been extracted to display that particular "page", remove the fieldset element 
  _form_manager_form_remove_fieldsets($form, $form_state);

  // set default values for the remaining form elements 
  _form_manager_form_default_values($form, $form_state);

  // add tabs to form
  _form_manager_form_add_tabs($form, $form_state);

  // add buttons to form
  _form_manager_form_add_buttons($form, $form_state);

  // add file support to form
  _form_manager_form_add_file_support($form, $form_state);

  return $form;
}


function _form_manager_form(&$form, &$form_state) {
  _form_manager_form_add_fields($form, $form_state);
  //return $form;
}


function _form_manager_form_add_fields(&$form, &$form_state, $form_id = '') {
  // TODO: Get the form_id passed as a parameter to this function
  if (empty($form_id)) {
    if (arg(1) == 'build') {
      $form_id = check_plain(arg(4));
    }
    else {
      $form_id = check_plain(arg(1));
    }
  }

  form_manager_get_form($form, $form_state, $form_id);
  _form_manager_convert_form($form, $form_id);
  
  form_manager_set_hidden_values($form, $form_state, $form_id);
}


function form_manager_get_form(&$form = array(), &$form_state, $form_id) {
  if (empty($form_id)) return FALSE;

  static $form_cache;

  // TODO: This is causing issues after initial submit of a form. Remove from caching for now.
  //if (empty($form_cache)) {
  if (1) {
    if (($cache = cache_get("form_manager-$form_id"))) {
      $form_cache = unserialize($cache->data);
      $form = $form_cache;
      
    }
    else {
      // Look in the database for the form
      $data = db_fetch_array(db_query("SELECT * FROM {form_manager_forms} WHERE form_id='%s'", $form_id));
      if (!empty($data)) {
        $form = unserialize($data['data']);
        #cache_set("form_manager-$form_id", 'cache', $data['data'], CACHE_TEMPORARY);
        cache_set("form_manager-$form_id", $data['data'], 'cache', CACHE_TEMPORARY);
        return $form;
      }
      
      // If not found in the database, look in an include file for the form
      else {
        // TODO: Enable a hook to allow other modules to include their form .inc files
        $file = drupal_get_path('module', 'form_manager') . "/forms/$form_id.inc";
        if (file_exists($file)) {
          // As much as I want to, module_load_include doesn't make the $form variable active
          //module_load_include('inc', 'form_manager', "forms/$form_id");
          include_once($file);
          #cache_set("form_manager-$form_id", 'cache', serialize($form), CACHE_TEMPORARY);
          cache_set("form_manager-$form_id", serialize($form), 'cache', CACHE_TEMPORARY);
          return $form;
        }
      }
    }
  }

  return $form_cache;
}


/**
 * Helper function to get the max number of steps in the forms, so that #navigation
 * can keep track of where the user is at and give prev/next buttons accordingly.
 */ 
function _form_manager_form_max_steps($form, &$form_state) {
  // calculate max steps if empty
  if (empty($form_state['storage']['#navigation']['maxSteps'])) {
     $fieldsetCount = 0;
     $otherFields = false;
       
    // loop through form elements
    foreach ($form as $k => $v) {
      // check for fieldset
      if (is_array($v) && $v['#type'] == 'fieldset') {
        $fieldsetCount++;
      } else {
        $otherFields = true;   
      }
    }
       
    // if there are other fields, increment max steps
    if ($otherFields > 1) {
      $fieldsetCount++;
      $form_state['storage']['#navigation']['otherFields'] = true;
    }
       
    // add count to storage
    $form_state['storage']['#navigation']['maxSteps'] = $fieldsetCount;
  }
}

function _form_manager_form_create_tabs(&$form, &$form_state) {
  // create list of tabs if not set in storage
  if (empty($form_state['storage']['#navigation']['tabs'])) {
    $tabsCount = 0;
    $tabs = array();
   
    // check for other fields
    if ($form_state['storage']['#navigation']['otherFields'] && $form_state['storage']['#navigation']['maxSteps'] > 1) {
      $tabsCount++;
      $tabs[$tabsCount] = "Start";
    }
       
    // loop through form items
    foreach ($form as $k => $v) {
      if (is_array($v) && $v['#type']=='fieldset') {
        $tabsCount++;
        $tabs[$tabsCount] = $v['#title'];
      }
    }
       
    // add tabs to storage
    $form_state['storage']['#navigation']['tabs'] = $tabs;   
       
  }
  
}


/**
 * Helper function to remove all form elements that aren't part of the current "page"
 */
function _form_manager_form_remove_fields(&$form, &$form_state) {
  // set fieldset counter based on otherfields
  if ($form_state['storage']['#navigation']['otherFields']) {
    $fieldsetCount = 1;
  } else {
    $fieldsetCount = 0;   
  }
   
  // loop though form fields
  foreach ($form as $k => $v) {
    // ensure this form item is a fieldset
    if (is_array($v) && $v['#type']=='fieldset') {

      // increment count of fieldsets
      $fieldsetCount++;

      // unset fieldset
      if ($form_state['storage']['#navigation']['step'] != $fieldsetCount) unset($form[$k]);
       
    } elseif (is_array($v) && $form_state['storage']['#navigation']['otherFields'] && $form_state['storage']['#navigation']['step'] > 1) {
      // unset field
      unset($form[$k]);
    }
  }
  $form_state['storage']['removed_fields'] = TRUE;
}


/**
 * Helper function to add remove the current page's fieldset wrapper so that only the form elements
 *   are presented to the user. If the top-level field element is a fieldset, loop through it to find
 *   all the children (and not properties, throw those away) and add those directly to the form. Then
 *   unset the fieldset once the children are safely removed.
 */ 
function _form_manager_form_remove_fieldsets(&$form, &$form_state) {
  foreach (element_children($form) as $child) {
    if ($form[$child]['#type'] == 'fieldset') {
      foreach (element_children($form[$child]) as $key) {
        if(element_child($form[$child])) {
          $form[$key] = $form[$child][$key];
        }
      }
      unset($form[$child]);
    }
  }
}


/**
 * Helper function to add the default values to the current form's page if the user has
 *   submitted anything. This is taken from form_state[storage'].
 */ 
function _form_manager_form_default_values(&$form, &$form_state, $parent = NULL) {
  $excludes = array('fieldset', 'password');
  
  // Some FAPI element types don't/can't have default_values
  if (!in_array($form['#type'], $excludes) && $form_state['storage'][$parent]) {
    $form['#default_value'] = $form_state['storage'][$parent];
  }

  // Recurse through the form or element to make sure that we set the default values on 
  // any child elements
  foreach (element_children($form) as $key => $child) {
    _form_manager_form_default_values($form[$child], $form_state, $child);
  }
}


function _form_manager_form_add_tabs(&$form, &$form_state) {
  $tabsArray = array();
  $tabsCount = 0;
   
  // loop through tabs and create buttons for each one
  foreach ($form_state['storage']['#navigation']['tabs'] as $k => $v) {
    $tabsCount++;
    $tabsArray['tab_' . $k] = array(
      '#type' => 'submit',
      '#value' => t($v),
      '#attributes' => array(
        'class' => 'tabs' . ($tabsCount == $form_state['storage']['#navigation']['step'] ? ' active' : '')
      ),
      // Set the weight to something really light so that the tab buttons will always be on top
      '#weight' => -50 + $tabsCount,
    );
  }
   
  // add tab buttons to beginning of form
  $form = array_merge($tabsArray, $form);
   
}

function _form_manager_form_add_buttons(&$form, &$form_state) {
  // add previous button
  if ($form_state['storage']['#navigation']['step'] > 1) {
    $form['previous'] = array(
      '#type' => 'submit',
      '#value' => t('Previous'),
      // Set the weight to something really heavy so that the previous/next/save buttons will always be on bottom
      '#weight' => 53,
    );       
  }
   
  // add next button
  if ($form_state['storage']['#navigation']['step'] < $form_state['storage']['#navigation']['maxSteps']) {
    $form['next'] = array(
      '#type' => 'submit',
      '#value' => t('Next'),
      '#weight' => 52, // ibid, see $form['previous']
    );       
  }

  // add save button
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
    '#weight' => 51, // ibid, see $form['previous']
  );
}

/**
 * Helper function to add file support to the form
 */ 
function _form_manager_form_add_file_support(&$form, &$form_state) {
  $form['#attributes'] = array('enctype' => 'multipart/form-data');
}


function form_manager_set_hidden_values(&$form, &$form_state, $form_id) {
  
}





